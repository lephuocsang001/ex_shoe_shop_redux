import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_QUANTITY, DELETE } from "../constant/ShoeContants";

class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleChangeQty(item.id, -1);
              }}
              className="btn btn-danger"
            >
              -
            </button>
            <strong>{item.soLuong}</strong>
            <button
              onClick={() => {
                this.props.handleChangeQty(item.id, +1);
              }}
              className="btn btn-success"
            >
              +
            </button>
          </td>
          <td>
            <img style={{ width: 50 }} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleDeleteItem(item.id);
              }}
              className="btn btn-danger"
            >
              Xoa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <h3 style={{ textAlign: "center" }}>Cart</h3>
        <table className="table">
          <thead>
            <tr>
              <td>ID</td>
              <td>Name</td>
              <td>Price</td>
              <td>So luong</td>
              <td>Image</td>
              <td>Action</td>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.ShoeReducer.cart,
  };
};

let mapStateToDispatch = (dispatch) => {
  return {
    handleDeleteItem: (id) => {
      let action = {
        type: DELETE,
        payload: id,
      };
      dispatch(action);
    },
    handleChangeQty: (id, soLuong) => {
      let action = {
        type: CHANGE_QUANTITY,
        payload: { id, soLuong },
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapStateToDispatch)(Cart);
