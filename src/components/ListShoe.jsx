import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

class ListShoe extends Component {
  render() {
    console.log(this.props);
    return (
      <div>
        <h3 style={{ textAlign: "center" }}>ListShoe</h3>
        <div className="row">
          {this.props.data.map((item, index) => {
            return <ItemShoe key={index} shoe={item} />;
          })}
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    data: state.ShoeReducer.data,
  };
};
export default connect(mapStateToProps)(ListShoe);
